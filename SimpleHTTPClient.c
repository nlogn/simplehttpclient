#include <stdio.h>
#include <stdlib.h>
#include "Winsock2.h"
#pragma comment(lib,"Ws2_32.lib")
#define HOST_LENGHT 64 // 호스트 최대길이
#define	URL_LENGHT 1024 // 전체 주소 최대길이
#define STATUS_LINE_LENGTH 64 // 상태 줄 최대 길이 (ex : HTTP/1.1 400 Bad Request)

void startup();
SOCKET getconnectedsocket(struct hostent*, short);
int sendHTTPrequest(SOCKET, char* host_url, char*);
int recvHTTPresponse(SOCKET, char*, char*, char*);
void cleanup();

int main()
{
	int res;

	char BUF[HOST_LENGHT + URL_LENGHT];

	char host[HOST_LENGHT];
	char host_url[HOST_LENGHT];
	short port;
	char request_uri[URL_LENGHT];

	char status_line[STATUS_LINE_LENGTH];
	int status_code;

	char origin_host[HOST_LENGHT];
	char redirect_BUF[HOST_LENGHT + URL_LENGHT] = "";

	struct hostent* host_info;
	SOCKET host_socket;

	// step 1. 파일이름 입력, 파일입출력 설정
	char in[32],out[32];
	printf("Enter the input file name: ");
	scanf("%s",in);
	printf("Enter the output file name: ");
	scanf("%s",out);
	freopen(in, "r", stdin);
	freopen(out, "w", stdout);
	

	
	startup();
	while (1)
	{
		//////////////////////////////////// redirect 예외처리
		if ( redirect_BUF[0] != NULL ) 
		{ 
			// redirect 주소로 변경.
			strcpy(BUF, redirect_BUF);
			printf("Location: %s\n", redirect_BUF);
			
		}
		else
		{
			memset(redirect_BUF, 0, sizeof(redirect_BUF));
			int order;
			if (gets(BUF) == NULL) break;
			if (BUF[0] == 0) continue;
			sscanf(BUF, "%d", &order);
			if (order < 0) order = 0;
			printf("%d. ", order);
		}


		// step 2. URL 읽기 및 파싱
		int p;
		memset(request_uri, 0, sizeof(request_uri));
		sscanf(BUF, "%*[^:]://%[^/]/%s", host_url, request_uri);
		if (strstr(host_url, ":") != NULL)
			sscanf(host_url, "%[^:]:%hd", host, &port);
		else
		{
			strcpy(host, host_url);
			port = 80;
		}
		if (redirect_BUF[0] == NULL) strcpy(origin_host, host); // redirect의 경우를 위해 원본 host이름 저장.
		

		// step 3. gethostbyname()으로 IP Address 얻기
		host_info = gethostbyname(host);
		if (host_info == NULL)
		{
			printf("gethostbyname() error\n");
			continue;
		}

		// step 4. 얻은 IP와 Port로 접속 시도하기
		host_socket = getconnectedsocket(host_info, port);
		if (host_socket == INVALID_SOCKET)
		{
			printf("socket() error\n");
			continue;
		}

		// step 5. HTTP request  전송하기
		res = sendHTTPrequest(host_socket, host, request_uri);
		if (res == SOCKET_ERROR)
		{
			printf("send() error\n");
			continue;
		}

		// step 6. HTTP response 받고 저장하기
		res = recvHTTPresponse(host_socket, host, redirect_BUF, origin_host);
		if (res == SOCKET_ERROR)
		{
			printf("recv() error\n");
			continue;
		}
	}
	cleanup();
	return 0;
}


void startup()
{
	WORD wVersionRequested = MAKEWORD(2, 2);
	WSADATA wsaData;
	WSAStartup(wVersionRequested, &wsaData);
	if (wsaData.wVersion != wVersionRequested)
	{
		printf("Invalid Version.\n");
		exit(0);
	}
}

SOCKET getconnectedsocket(struct hostent* host_info, short port)
{
	int nRet;
	SOCKET host_socket;
	SOCKADDR_IN hostaddr_in;
	host_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	hostaddr_in.sin_family = AF_INET;
	hostaddr_in.sin_addr = *((LPIN_ADDR)*host_info->h_addr_list);
	hostaddr_in.sin_port = htons(port);
	nRet = connect(host_socket, (LPSOCKADDR)&hostaddr_in, sizeof(struct sockaddr));
	if (nRet == SOCKET_ERROR)
	{
		closesocket(host_socket);
		host_socket = INVALID_SOCKET;
	}
	return host_socket;
}

int sendHTTPrequest(SOCKET host_socket, char* host_url, char* request_uri)
{
	/*
	Sample request
	GET /w/icon/1312/19/152729032.png HTTP/1.1
	Host: icon.daumcdn.net
	*/
	int nRet;
	char* get_request;
	int len = strlen(request_uri) + 100;
	get_request = (char*)malloc(len);

	strcpy(get_request, "GET /");
	strcat(get_request, request_uri);
	strcat(get_request, " HTTP/1.1");
	strcat(get_request, "\r\n");

	strcat(get_request, "Host: ");
	strcat(get_request, host_url);
	strcat(get_request, "\r\n");

	//strcat(get_request, "connection: close");
	//strcat(get_request, "\r\n");

	strcat(get_request, "\r\n");

	nRet = send(host_socket, get_request, strlen(get_request), 0);
	if (nRet == SOCKET_ERROR)
		closesocket(host_socket);
	free(get_request);
	return nRet;
}
int recvHTTPresponse(SOCKET host_socket, char* host_url, char* redirect_url, char* origin_host)
{
	/*
	- status code를 해석하기
	- redirection의 경우 new site에 접속해서 받아오기 (status code가 3**인 경우)
	- header field에 명시된 방식으로 body의 크기를 계산하여 body를 모두 수신하기
	- header에 "Content-Length:" field가 있으면 이를 해석해서 body 크기를 알아냄
	- header에 “Transfer-Encoding: chunked” 라고 명시된 경우, body 내의 response의 크기를 차근차근 계산해야 함
	7) 202 OK response의 경우, 수신한 body를 c:/temp에 별도의 file로 저장하고, file 이름은 URL 내의 host와 file extension의 조합으로 함
	*/
#define BUFFER_SIZE 1		// recv 처리버퍼 (보통 2048)
#define HEADER_SIZE 4096	// 헤더인자 처리버퍼 (헤더필드의 최대 길이. 구분자까지의 최대길이)
#define PATH "c:\\temp\\"  // 저장경로 설정

	// 버퍼 처리용
	int nRet;
	char buf[BUFFER_SIZE];
	int buf_size;
	char* buf_pos;

	// 헤더 처리용
	char header[HEADER_SIZE];
	int header_len = 0;

	// 상태코드용
	int is_status_line = 1;
	char status_line[64];
	int status_code;
	int is_2xx = 0;

	// 데이터 처리용
	int is_data = 0;
	int body_len = 0;
	int content_length = 0;
	char transfer_encoding[16];
	int closed = 0;
	

	// 파일 입출력 처리용
	FILE *fp;
	char path[64] = PATH;
	char file_extension[16];
	char file_buffer[BUFFER_SIZE + 1];
	int file_buffer_len = 0;


	// chunked 처리용
	int chunked_mode = 0;
	char chunk_buffer[16];
	int chunk_buffer_len = 0;
	int is_start_of_chunk = 1;
	int chunk_size = 0;
	int end_r = 0, end_n = 0;
	

	strcat(path, origin_host);
	while (1)
	{
		memset(buf, 0, sizeof(buf));
		nRet = recv(host_socket, buf, sizeof(buf), 0);
		if (nRet == SOCKET_ERROR)
			break;
		if (nRet == 0)
			break;
		buf_size = nRet;
		buf_pos = buf;
		while (is_data == 0)// 헤더인 경우에만
		{
			/* \r\n을 recv 한번에 못받는 경우를 처리하기 위해 버퍼의 내용을 다른 버퍼에 옮겨서 처리한다. 	*/
			header[header_len++] = *buf_pos++;
			if (header_len >= 2 &&
				header[header_len - 2] == '\r' &&
				header[header_len - 1] == '\n')
			{
				if (header_len == 2) // 헤더의 끝
				{
					if (is_2xx)
					{
						strcat(path, ".");
						strcat(path, file_extension);
						fp = fopen(path, "wb");
					}
					is_data = 1;
				}
				else
				{
					if (is_status_line) // status line인 경우
					{
						header[header_len-2] = 0;
						strcpy(status_line, header);
						sscanf(status_line, "%*s %d %*s", &status_code);
						if (status_code >= 200 && status_code < 300) is_2xx = 1;
						is_status_line = 0;
					}
					else
					{
						// 헤더 인자 해석
						char* p;
						for (;;)
						{
							p = strstr(header, "Content-Length:");
							if (p == header)
							{
								sscanf(header, "%*[^:]: %d", &content_length);
								break;
							}
							p = strstr(header, "Transfer-Encoding:");
							if (p == header)
							{
								sscanf(header, "%*[^:]:%s", transfer_encoding);
								if (strcmp(transfer_encoding, "chunked") == 0) chunked_mode = 1;
								break;
							}

							p = strstr(header, "Content-Type:");
							if (p == header)
							{
								sscanf(header, "%*[^:]:%*[^/]/%[^;\r\n]", file_extension);
								break;
							}

							p = strstr(header, "Location:");
							if (p == header)
							{
								sscanf(header, "%*[^:]:%s", redirect_url);
								break;
							}
							break;

						}
					}
					memset(header, 0, sizeof(header));
					header_len = 0;
				}
			}
			if (buf_pos >= buf + buf_size) break;
		}
		/////////////////////////////////// 헤더끝남



		/////////////////////////////////// 여기서부터 데이터 처리
		while (is_data && buf_pos < buf + buf_size)
		{
			if (chunked_mode)
			{
				if (is_start_of_chunk ) // chunk의 시작이면 chunk의 사이즈 받기
				{
					while (buf_pos < buf + buf_size)
					{
						chunk_buffer[chunk_buffer_len++] = *buf_pos++;
						if (chunk_buffer_len >= 2 &&
							chunk_buffer[chunk_buffer_len - 2] == '\r' &&
							chunk_buffer[chunk_buffer_len - 1] == '\n')
						{
							chunk_buffer[chunk_buffer_len - 2] = 0;
							chunk_size = strtol(chunk_buffer,NULL, 16);
							memset(chunk_buffer, 0, sizeof(chunk_buffer));
							is_start_of_chunk = 0;
							chunk_buffer_len = 0;
							if (chunk_size == 0)
							{
								closesocket(host_socket);
								closed = 1;
							}

							break;
						}
					}
				}
				if(is_start_of_chunk == 0) // chunk의 시작이 아니고 받다가 중간에 짤렸을 경우 계속 이어받기
				{
					while (body_len < chunk_size)
					{
						if (buf_pos >= buf + buf_size)
						{
							break;
						}
						body_len++;
						file_buffer[file_buffer_len++] = *buf_pos++;
						
					}
					if (body_len >= chunk_size)
					{
						// 찌꺼기 \r\n 받아야됨... 
						if (buf_pos < buf + buf_size) // HTTP chuncked 규칙에 따라 \r, \n이 무조건 나타난다고 가정함. 
						{
							if (*buf_pos == '\r')
							{
								buf_pos++;
								end_r = 1;
							}
						}
						if (buf_pos < buf + buf_size)
						{
							if (*buf_pos == '\n')
							{
								buf_pos++;
								end_n = 1;
							}
						}
						if ( end_r && end_n )// 전부 받았을 경우
						{
							//printf("%d\n", chunk_size);
							body_len = 0;
							end_r = end_n = 0;
							is_start_of_chunk = 1;
						}
					}
				}

			}
			else
			{
				while (buf_pos < buf + buf_size)
				{
					if (content_length > 0 && body_len >= content_length) break;
	
					file_buffer[file_buffer_len++] = *buf_pos++;
					body_len++;
				}
				if (content_length <= body_len)
				{
					closesocket(host_socket);
					closed = 1;
				}

			}

			
			file_buffer[file_buffer_len] = 0;
			if (file_buffer_len > 0)
			{
				if (is_2xx) // 200번대 상태코드만 파일입출력
					fwrite(file_buffer, file_buffer_len, 1, fp);
			}
			file_buffer_len = 0;
			memset(file_buffer, 0, sizeof(file_buffer));
			if (buf_pos >= buf + buf_size) break;
		}
	}
	// 상태 출력
	
	if(status_code==200 && redirect_url[0]!=0) // ?? redirect로 다시 접속한 경우 status는 출력하지 않는..
		redirect_url[0] = 0; 
	else puts(status_line);
	if (is_2xx) 
		fclose(fp);
	closesocket(host_socket);
	if (closed == 0) nRet = SOCKET_ERROR;
	else nRet = 0;
	return nRet;
}

void cleanup()
{
	WSACleanup();
}